public class Pilotar {
	private static Pilotar instance;
	private Pilotar() {
	}
	public static Pilotar getInstance() {
		if(instance == null) {
			instance = new Pilotar();
		}
		return instance;
	}
	public static void Acelerar() {
		System.out.println("Moto aumenta a velocidade...");
	}
	public static void VirarEsquerda() {
	System.out.println("Moto vira para Esquerda...");
	}
	public static void VirarDireita() {
		System.out.println("Moto vira para Direita...");
	}
	public static void Frear() {
		System.out.println("Moto Para...");
	}
}
