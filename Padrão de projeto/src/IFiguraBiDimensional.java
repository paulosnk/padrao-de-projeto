
public interface IFiguraBiDimensional {
	public double perimetro();
	public double area();
}