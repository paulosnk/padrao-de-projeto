
public class Honda extends Moto 
{
	public Honda (String type, String motoMaker, double kilometerL) {
		setType(type);
		setmotoMaker(motoMaker);
		setKilometerL(kilometerL);
	}
	private void setmotoMaker(String motoMaker) {
		// TODO Auto-generated method stub
		
	}
	public Honda (String motoMaker, double kilometerL) {
		this.motoMaker = motoMaker;
		this.kilometerL = kilometerL;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setCarMaker(String motoMaker) {
		this.motoMaker = motoMaker;
	}
	public void setKilometerL(double kilometerL) {
		this.kilometerL = kilometerL;
	}
	public String getType() {
		return type;
	}
	public String getCarMaker() {
		return motoMaker;
	}
	public double getKilometerL() {
		return kilometerL;
	}
}
