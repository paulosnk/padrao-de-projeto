public class Triangulo implements IFiguraBiDimensional{
	private int ladoA;
	private int ladoB;
	private int ladoC;
	private boolean condicaoExistencia(int a, int b, int c) {
		return Math.abs(b - c) < a && a < b + c;
	}
	public Triangulo(int a) {
		if(a > 0) {
			this.ladoA = a;
			this.ladoB = a;
			this.ladoC = a;
		}else {
			this.ladoA = 1;
			this.ladoB = 1;
			this.ladoC = 1;
		}
			}
	public Triangulo(int a, int b, int c) {

		if (!condicaoExistencia(a, b, c)) {
			throw new RuntimeException("Impossvel construir tringulo");
		}
		this.ladoA = a;
		this.ladoB = b;
		this.ladoC = c;
	}
	@Override
	public String toString() {
		return "(" + this.ladoA + ", " + this.ladoB + ", " + this.ladoC + ")";
	}
	public int getLadoA() {
		return ladoA;
	}
	public void setLadoA(int a) {
		if (condicaoExistencia(a, ladoB, ladoC)) {
			this.ladoA = a;
		}
	}
	public int getLadoB() {
		return ladoB;
	}
	public void setLadoB(int b) {
		if (condicaoExistencia(ladoA, b, ladoC)) {
			this.ladoB = b;
		}
	}
	public int getLadoC() {
		return ladoC;
	}
	public void setLadoC(int c) {
		if (condicaoExistencia(ladoA, ladoB, c)) {
			this.ladoC = c;
		}
	}
	public double perimetro() {
		return ladoA + ladoB + ladoC;
	}
	public double area() {
		double s = perimetro() / 2;	
		double sa = s * (s - ladoA) * (s - ladoB) * (s - ladoC);	
		return Math.sqrt(sa);
	}
}
