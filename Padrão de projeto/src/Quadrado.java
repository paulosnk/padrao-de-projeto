
public class Quadrado implements IFiguraBiDimensional {
	private int lado;
	private boolean condicaoExistencia(int l) {
		return l > 0;
	}
	public Quadrado() {
		this.lado = 1;
	}
	public Quadrado(int lado) {
		if(!condicaoExistencia(lado)) {
			throw new RuntimeException("Impossivel construir Quadrado. ");
		}
		this.lado = lado;
	}
	public int getLado() {
		return lado;
	}
	public void setLado(int lado) {
		if(condicaoExistencia(lado)) {
			this.lado = lado;
		}
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return lado*4;
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return lado*lado;
	}

}
