
public class FactoryMoto 
{
		public Moto getMoto(String type, String motoMaker, double kilometerL) {
			if(type.equals("Naked")) {
				return new Yamaha(motoMaker, kilometerL);
			}
			if(type.equals("Custom")) {
				return new harleydavidson(motoMaker, kilometerL);
			}
			if(type.equals("Street")) {
				return new Honda(motoMaker, kilometerL);
			}
			if(type.equals("Sport")) {
				return new Kansas(motoMaker, kilometerL);
			}
			return null;
		}

}
