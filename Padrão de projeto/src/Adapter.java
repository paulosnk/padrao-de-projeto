import javax.swing.JOptionPane;

public class Adapter implements IExTarget{
	private adaptee adaptee;
	
	public Adapter() {
		adaptee = new adaptee();
	}
	
	public void exibirMensagem() {
		
		
		String num = JOptionPane.showInputDialog(null, "Tipo de mensagem:"
				+ "\n(0 - ERROR) "
				+ "\n(1 - INFORMATION)"
				+ "\n(2 - WARNING)");
		int numero = Integer.parseInt(num); 
	
		String msg = JOptionPane.showInputDialog(null, "Qual  a mensagem?", numero);
		
		String tipo;
		switch(numero) {
		case 0:
			tipo = "ERR";
			break;
		case 2:
			tipo = "WARN";
			break;
		default:
			tipo = "";
			break;
		}
		adaptee.enviarMensagem(msg, tipo);
	}

}
